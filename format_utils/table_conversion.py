# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np


def convert_df_to_md(dataframe):
    column_widths = dataframe.apply(lambda x: x.str.len().max().astype(int))
    header_widths = list(map(len, dataframe.columns))
    column_widths = np.max([column_widths, header_widths], axis=0)

    lines = list()
    lines.append(_concat(_just(dataframe.columns, column_widths)))
    lines.append(_concat(_create_header_sep(column_widths)))
    for ind, row in dataframe.iterrows():
        lines.append(_concat(_just(row, column_widths)))

    return '\n'.join(lines)

def _create_header_sep(widths, sep='-'):
    line = [sep * w for w in widths]
    return line

def _just(cells, widths):
    return [str(c).ljust(w) for c, w in zip(cells, widths)]

def _concat(cells, sep=' | ', head='| ', tail=' |'):
    line = head + sep.join(cells) + tail
    return line
