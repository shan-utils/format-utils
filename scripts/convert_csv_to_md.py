#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

desc = 'Convert .tsv/.csv to markdown table'
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('-i', '--input', required=True, help='Input .csv/.tsv file')
parser.add_argument('-o', '--output', help='The output markdown table file')
parser.add_argument('-s', '--delimiter', help='The file delimiter (, or \\t)',
                    default=',')
args = parser.parse_args()

import pandas as pd

from format_utils import convert_df_to_md


df = pd.read_csv(args.input, sep=args.delimiter)
output = convert_df_to_md(df)

if args.output is not None:
    with open(args.output, 'w') as output_file:
        output_file.write(output)
else:
    print(output)
