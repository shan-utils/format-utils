# -*- coding: utf-8 -*-

from distutils.core import setup
from glob import glob
import subprocess

scripts = glob('scripts/*')
command = ['git', 'describe', '--tags']
version = subprocess.check_output(command).decode().strip()

setup(name='format-utils',
            version=version,
            description='Some formatting utils',
            author='Shuo Han',
            author_email='shan50@jhu.edu',
            scripts=scripts,
            install_requires=['pandas'],
            packages=['format_utils'])
